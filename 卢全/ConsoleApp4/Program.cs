﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = new Hashtable();
           
            hashtable.Add(1, "小猪giao奇");//增加元素
            hashtable.Remove(1);//根据指定的Key值移除对应的集合元素
            hashtable.Clear();//清空所有元素
            hashtable.ContainsKey(1);//判断key值里是否还有1
            hashtable.ContainsValue("小猪giao奇");//判断Value值里是否含有“小猪giao奇”




            Hashtable ht = new Hashtable();
            ht.Add(1, "小明");
            ht.Add(2, "小红");
            ht.Add(3, "小刚");
            Console.WriteLine("学生编号 ");
            int id = int.Parse(Console.ReadLine());
            bool flag = ht.ContainsKey(id);
            if (flag)
            {
                Console.WriteLine("查找的学生姓名为：{0}", ht[id].ToString());
            }
            else
            {
                Console.WriteLine("该学生不存在！");
            }
            Console.WriteLine("所有的学生如下：");
            foreach (DictionaryEntry w in ht)
            {
                int key = (int)w.Key;
                string value = w.Value.ToString();
                Console.WriteLine("学生编号：{0}，学生姓名：{1}", key, value);
            }
        }
    }
}
